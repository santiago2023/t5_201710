package generadorDatos;

import java.util.Random;

public class GeneradorDatos {

	public String generadorStrings(){
		int num1 = 97;
		int num2 = 122;
		String palabra = "";
		char c = 0;
		for (int i=1; i<=10; i++){
			int numAleatorio = (int)Math.floor(Math.random()*(num2 -num1)+num1);
			char letra =(char)numAleatorio;
			palabra = palabra+ letra;
			c++;
		}
		return palabra;
	}

	public int generadorInt(){
		Random rnd = new Random();

		return rnd.nextInt()*2017 + 1950; 
	}

}
