package model.data_structures;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;

import generadorDatos.VOPelicula;

public class PriorityQueue {
	private static class ComparadorP
	{
		
		public int compare(VOPelicula o1, VOPelicula o2) {
			// TODO Auto-generated method stub
			if(o1.getAgnoPublicacion()==o2.getAgnoPublicacion()){
				return o1.getTitulo().compareTo(o2.getTitulo());
			}
			return o1.getAgnoPublicacion()-o2.getAgnoPublicacion();
		}


	
	}
	private ComparadorP myComp = new ComparadorP();
	private int        tamano;
	private ArrayList<VOPelicula>  myList;

	
	private class PQItr implements Iterator
	{
		public Object next()
		{
			return myList.get(myCursor);
		}

		public boolean hasNext()
		{
			return myCursor <= tamano;
		}

		public void remove()
		{
			throw new UnsupportedOperationException("remove not implemented");
		}

		private int myCursor = 1;
	}



	
	public PriorityQueue()
	{
		myList = new ArrayList<VOPelicula>(1000);
		myList.add(null);             // first slot has index 1
		tamano = 0;
	}

	/**
	 * constructs an empty priority queue, when new elements
	 * are added the <code>Comparator comp</code> determines which is
	 * smaller.
	 *
	 * @param comp is the <code>Comparator</code> used in determining order
	 */

	public PriorityQueue(ComparadorP comp)
	{
		this();
		myComp = comp;
	}

	
	public PriorityQueue(Collection coll)
	{
		this();
		myList.addAll(coll);
		tamano = coll.size();

		for(int k=coll.size()/2; k >= 1; k--)
		{
			heapify(k);
		}
	}

	

	public boolean add(VOPelicula o)
	{
		myList.add(o);        // stored, but not correct location
		tamano++;             // added element, update count
		int k = tamano;       // location of new element

		while (k > 1 && myComp.compare(myList.get(k/2), o) > 0)
		{
			myList.set(k, myList.get(k/2));
			k /= 2;
		}
		myList.set(k,o);

		return true;
	}

	
	public int size()
	{
		return tamano;
	}

	
	public boolean isEmpty()
	{
		return tamano == 0;
	}

	

	public Object remove()
	{
		if (! isEmpty())
		{
			Object actual = myList.get(1);

			myList.set(1, myList.get(tamano));  // move last to top
			myList.remove(tamano);              // pop last off
			tamano--;
			if (tamano > 1)
			{
				heapify(1);
			}
			return actual;
		}
		return null;
	}

	

	public Object peek()
	{
		return myList.get(1);
	}


	

	private void heapify(int raiz)
	{
		VOPelicula last = myList.get(raiz);
		int hijo, k = raiz;
		while (2*k <= tamano)
		{
			hijo = 2*k;
			if (hijo < tamano &&
					myComp.compare(myList.get(hijo),
							myList.get(hijo+1)) > 0)
			{
				hijo++;
			}
			if (myComp.compare(last, myList.get(hijo)) <= 0)
			{
				break;
			}
			else
			{
				myList.set(k, myList.get(hijo));
				k = hijo;
			}
		}
		myList.set(k, last);
	}



}
