package model.data_structures;

public class NodosEnlazados<T> {
	private T item;
	private NodosEnlazados<T> siguiente;
	public T darItem(){
		return item;
	}
	public NodosEnlazados<T> darSiguiente(){
		return siguiente;
	}
	public void modificarSiguiente(NodosEnlazados<T> pSiguiente){
		siguiente=pSiguiente;
	}
	public void modificarItem(T pItem){
		item=pItem;
			
	}
	

}
