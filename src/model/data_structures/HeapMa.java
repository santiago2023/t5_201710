package model.data_structures;

import generadorDatos.VOPelicula;

public class HeapMa {

	private int capacity;

	private int last;

	private Object S[];



	public void crearCP(int i){
		S = new Object[i+1];
		last = 0;
		capacity = i;
	}

	public int darNumeroDeElementos(){
		return capacity;
	}

	public void agregarElemento(VOPelicula elemento) throws Exception{
		if(tamanoMax() == capacity){
			throw new Exception("Heap overflow");
		}else{
			last++;
			S[last] = elemento;
			upHeapBubble();
		}

	}

	public VOPelicula max() throws Exception{
		if(esVacia()) throw new Exception("The heap is empty");
		else {
			VOPelicula t = (VOPelicula) S[tamanoMax()];
			return t;
		}
	}

	public boolean esVacia(){
		if( tamanoMax() != 0) return true;
		else return false;
	}

	public int tamanoMax(){
		return last;
	}

	private int compare(Object x , Object y){
		
		if (((VOPelicula) x).getAgnoPublicacion()==((VOPelicula)y).getAgnoPublicacion()){
			return ((VOPelicula)x).getTitulo().compareTo(((VOPelicula)y).getTitulo());
		}
		return ((VOPelicula) x).getAgnoPublicacion()-((VOPelicula)y).getAgnoPublicacion();
	}

	public void upHeapBubble(){
		int index = tamanoMax();
		while(index>1){
			int parent = index /2;
			if(compare(S[index], S[parent])>=0){
				break;
			}
			swap(index, parent);
			index = parent;
		}
	}


	public void swap(int i , int j){
		Object temp = S[i];
		S[i] = S[j];
		S[j] = temp;
	}
}
