package test;

import generadorDatos.VOPelicula;
import junit.framework.TestCase;
import maejador.ManejadorPeliculas;
import model.data_structures.HeapMa;
import model.data_structures.ListaEncadenada;
import model.data_structures.PriorityQueue;

public class Prueba extends TestCase {

	private ManejadorPeliculas manejador;
	public void setupEscenario1(){
		manejador= new ManejadorPeliculas();
		
	}

public void testCrearCola(){
	 setupEscenario1();
	 PriorityQueue cola= manejador.crearColaP();
	 System.out.println(cola);
	 assertTrue(cola.isEmpty());
	 assertEquals(cola.size(),0);
	 assertNull(cola.remove());
}
public void testCrearMonticulo(){
	setupEscenario1();
	HeapMa monticulo=manejador.crearMonticuloCP();
	System.out.println(monticulo);
	assertEquals(monticulo.darNumeroDeElementos(),0);
	
}
public void testCargarPeliculas(){
	setupEscenario1();
	assertTrue(manejador.cargarPeliculasSR("./data/movies.csv"));
}

}
