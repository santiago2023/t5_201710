package maejador;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import generadorDatos.VOPelicula;
import model.data_structures.*;
import model.data_structures.ListaEncadenada;



public class ManejadorPeliculas {
	//En lugar de usar la clase VOPreferencia, usamos la clase VOPelicula ya existente porque nos parece m�s completo.
	private PriorityQueue cola;
	public boolean cargarPeliculasSR(String archiVOUsuarioConteo) {
		// TODO Auto-generated method stub
		String generoBuscado ="Animation"; //En el enunciado no se es claro cu�l es el g�nero determinado. No dice si entra por par�metro, por eso lo elegimos
		BufferedReader br = null;

		try {
			cola= new PriorityQueue();


			br =new BufferedReader(new FileReader(archiVOUsuarioConteo));
			br.readLine();
			String line=br.readLine();
			String[] x;
			

			while (!line.contains("EOF")) {


				VOPelicula pelicula=new VOPelicula();
				int indice2=line.indexOf(",");
				String campos=line.substring(indice2+1);
				int indice = campos.lastIndexOf("(");
				String campos2=campos.substring(indice+1);
				String[] campos3=campos2.split("\\),");
				String[] campos4=campos.split("\\(");

				String nombre=campos4[0];
				String ano=campos3[0];
				String generos=campos3[1];
				
				
				
				
				pelicula.setTitulo(nombre.trim());
				pelicula.setAgnoPublicacion(Integer.parseInt(ano));
			
				String []generosArreglo=generos.split("|");
				for (int i=0;i<generosArreglo.length;i++){
					if(generosArreglo[i].equalsIgnoreCase(generoBuscado)){
						cola.add(pelicula);
					}
			
			

				
				cola.add(pelicula);

				line = br.readLine();

			}


			}
		
			}
		 catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			if (null!=br) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();

				}
			}
		}


		return true;


	}

		public PriorityQueue crearColaP(){
			// Como mencionamos, si aqu� pusieramos el par�metro, habr�a que modificar de forma dr�stica c�mo implementamos el heap. Para ver c�mo se crea en �ltima instancia la cola de prioridad, ver el constructor de PriorityQueue
			PriorityQueue colaRetorno= new PriorityQueue();
			return colaRetorno;
			
		}
		public HeapMa crearMonticuloCP(){
			HeapMa monticulo= new HeapMa();
			System.out.println(monticulo);
			return monticulo;
		}
		
	}

